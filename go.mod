module gitlab.com/kondor6c/events_process_service

go 1.15

require (
	github.com/go-redis/redis/v8 v8.10.0
	github.com/julienschmidt/httprouter v1.3.0
)
