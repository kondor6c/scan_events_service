package main

import (
	"context"
	"database/sql"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	"golang.org/x/time/rate"

	"github.com/go-redis/redis/v8"
	"github.com/golang-jwt/jwt"
	"github.com/julienschmidt/httprouter"
)

type Scan struct {
	FilePath    string    `json:"path,omitempty"  db:"path"`
	FileSize    string    `json:"size,omitempty db:"size"`
	Data        []byte    `json:"b64_string,omitempty"`
	MultiData   []string  `json:"b64_lines,omitempty"`
	User        string    `json:"user,omitempty"  db:"user"`
	Description string    `json:"description,omitempty"  db:"description"`
	Bulk        string    `json:"bulk,omitempty"  db:"bulk"`
	Date        time.Time `json:"added,omitempty"  db:"added"`
}

type AppError struct {
	Status int16       `json:"status"`
	Err    interface{} `json:"err_text"`
}

type UserInfo struct {
	mu       *sync.RWMutex
	Uid      int    `json:"uid"`
	Username string `json:"username"`
	Password string `json:"password"`
	Gid      int    `json:"gid"`
	Limit    rate.Limit
	Current  int
	Burst    int
	Active   bool
}

type JsonResponse struct {
	Scan     interface{} `json:"sent"`
	Id       int         `json:"id,omitempty"  db:"id"`
	Detect   string      `json:"scan_detect,omitempty"  db:"scan_detect"`
	Duration int         `json:"duration,omitempty"  db:"duration"`
	Result   string      `json:"scan_result,omitempty"  db:"scan_result"`
	Status   string      `json:"scan_status,omitempty"  db:"scan_status"`
	AppErr   AppError    `json:"app_error,omitempty"`
}

var evdb *redis.Client
var db *sql.DB

func pubScan(s *Scan) {
	ctx := context.Background()

	strScan, _ := json.Marshal(s)
	log.Println("string-ized")
	log.Println(strScan)
	err := evdb.Publish(ctx, "filescan", string(strScan)).Err()
	log.Printf("published %v", s)
	if err != nil {
		log.Panicln(err)
	}
}

func GetUser(dB *sql.DB, u UserInfo) (user UserInfo, rerr error) {
	var qUserB string = "SELECT uid, gid, username, password FROM user WHERE "
	var qUserName string = qUserB + " username LIKE ?"

	fetch, err := db.Query(qUserName, u.Username)
	if err != nil {
		rerr = err
	}
	for fetch.Next() {
		fetch.Scan(&user.Uid, &user.Gid, &user.Username, &user.Password)
	}
	return
}

var NotImplemented = httprouter.Handle(func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Write([]byte("Not Implemented, look at extra credit"))
})

func CheckUser(u UserInfo) (pass bool, rerr error) {
	user, lookupErr := GetUser(db, u)
	// try to see if the password is hashed
	if _, isHashed := hex.DecodeString(u.Password); isHashed == nil {
		if user.Password == u.Password && lookupErr == nil {
			pass = true
		} else if lookupErr != nil {
			rerr = lookupErr
		}
	} else {
		pass = false
		rerr = isHashed
	}
	return
}

func NewItem(s *Scan) error {
	strScan, _ := json.Marshal(s)
	e := evdb.LPush(context.Background(), "scans", string(strScan)).Err()
	log.Println("adding to redis")
	log.Println(e)
	return e
	//evdb.Do("EXPIRE", key, ttl)
}

func LookItem(id int64) (found Scan, rerr error) {
	ctx := context.Background()
	val, err := evdb.LIndex(ctx, "scans", id).Result()
	switch {
	case err == redis.Nil:
		rerr = errors.New("key does not exist")
	case err != nil:
		rerr = errors.New("failed to obain")
	case val == "":
		rerr = errors.New("value is empty")
	}
	if err := json.Unmarshal([]byte(val), found); err != nil {
		rerr = err
	}
	return
}

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	response := &JsonResponse{}
	fmt.Fprint(w, "read docs, printing all in redis\n")
	response.Result = "index"
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(response); err != nil {
		log.Panicln(err)
	}
}

/*
func Login(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var user userInfo
	response := &JsonResponse{}

	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		response.AppErr = AppError{Status: 405, Err: "Method Not Allowed"}
		// Failed to parse the json
		if responseErr := json.NewEncoder(w).Encode(response); err != nil {
			response.AppErr = AppError{Status: 405, Err: "Method Not Allowed"}
			log.Println("json encoding error:")
			log.Panicln(responseErr)
			log.Println("the incoming json error:")
			log.Panicln(err)
			// our side, Failed to return encoded json to the user
		}
		return
	}
	if passCheck, err := CheckUser(user); err == nil && passCheck == true {
		w.WriteHeader(http.StatusNotFound)
		log.Println("password is good and no errors in the query")
		return
	}
}
*/

// I might be noticing an increasingly need for middleware
// to facilitate byte size, rate limits, auth, denials around IPs
// https://dev.to/plutov/rate-limiting-http-requests-in-go-based-on-ip-address-542g
func rateLimit(h httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var curLimit = rate.NewLimiter(1, 3)
		remoteIP := r.Header.Get("REMOTE_ADDR")
		log.Println(remoteIP)
		if !curLimit.Allow() {
			w.WriteHeader(429)
		} else {
			h(w, r, ps)
		}
	}
}

func TokenAuth(h httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		user := &UserInfo{}
		response := &JsonResponse{}

		if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
			response.AppErr = AppError{Status: 405, Err: "Method Not Allowed"}
			// Failed to parse the json
			if responseErr := json.NewEncoder(w).Encode(response); err != nil {
				response.AppErr = AppError{Status: 405, Err: "Method Not Allowed"}
				log.Println("json encoding error:")
				log.Panicln(responseErr)
				log.Println("the incoming json error:")
				log.Panicln(err)
				// our side, Failed to return encoded json to the user
				return
			}
		}
		if passCheck, err := CheckUser(*user); err == nil && passCheck == true {
			w.WriteHeader(http.StatusNotFound)
			log.Println("password is good and no errors in the query")
			h(w, r, ps)
		}
	}
}

func NewToken(u UserInfo) (token string, rerr error) {

	jwtClaim := jwt.MapClaims{}
	jwtClaim["authorized"] = true
	jwtClaim["user_id"] = u.Uid
	jwtClaim["group"] = u.Gid
	jwtClaim["limit"] = u.Limit
	jwtClaim["current"] = u.Current
	jwtClaim["exp"] = time.Now().Add(time.Minute * 60).Unix()
	atJwt := jwt.NewWithClaims(jwt.SigningMethodHS256, jwtClaim)
	token, rerr = atJwt.SignedString([]byte(os.Getenv("SHARED_SECRET")))
	return
}

// new virus scan of file return ID
// POST /scan
func NewScan(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	log.Printf("POST request for /scan ")
	/*
		var ctx context.Context
		var cancel context.CancelFunc
		ctx, cancel := context.WithTimeout(r.Context(), time.Duration(30*time.Second))
		defer cancel()
		reqToken := r.Header.Get("Authorization")
		splitToken := strings.Split(reqToken, "Bearer ")
		reqToken = splitToken[1]
	*/
	fScan := &Scan{}
	response := &JsonResponse{}

	full, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		log.Panicln(err)
		log.Printf("error reading incoming request body to memory")
	}
	if err := json.Unmarshal(full, &fScan); err != nil {
		log.Println("error at json, responding as such")
		response.AppErr = AppError{Status: 405, Err: "Method Not Allowed"}

		if err := json.NewEncoder(w).Encode(response); err != nil {
			// For us to review later
			log.Panicln(err)
		}
	}

	go pubScan(fScan)
	go NewItem(fScan)
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(response); err != nil {
		log.Panicln(err)
	}
}

// GET /scan/:id
func GetScan(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	//w.Header().Set("Access-Control-Allow-Origin", "*")
	iId, _ := strconv.Atoi(params.ByName("id"))
	response := &JsonResponse{}
	if scanId, ok := LookItem(int64(iId)); ok != nil {
		w.WriteHeader(http.StatusNotFound)
		response.AppErr = AppError{Status: 404, Err: "Item Not Found"}
		if err := json.NewEncoder(w).Encode(response); err != nil {
			log.Panicln(err)
		}
	} else {
		// caution, might not be hit
		response.Scan = scanId
	}
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(response); err != nil {
		log.Panicln(err)
	}
}

/*
var NotImplemented = httprouter.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Not Implemented"))
})
*/

func main() {
	evPass := os.Getenv("EVPASS")
	evHost := os.Getenv("EVHOST")
	evdb = redis.NewClient(&redis.Options{
		Addr:     evHost,
		Password: evPass,
		DB:       0,
	})

	router := httprouter.New()
	router.GET("/", Index)
	router.GET("/user", NotImplemented)
	router.GET("/scan/:id", GetScan)
	router.POST("/scan", NewScan)
	appPort := os.Getenv("APP_PORT")
	if len(appPort) < 1 {
		appPort = ":5000"
	}
	log.Fatal(http.ListenAndServe(appPort, router))
}
