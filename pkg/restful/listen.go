package main

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/julienschmidt/httprouter"
)

type Scan struct {
	Id          int       `json:"id,omitempty"  db:"id"`
	FilePath    string    `json:"file,omitempty"  db:"file"`
	FileSize    string    `json:"file_size,omitempty db:"name"`
	Data        string    `json:"base64_data,omitempty"`
	User        string    `json:"user,omitempty"  db:"user"`
	Description string    `json:"description,omitempty"  db:"description"`
	Bulk        string    `json:"bulk,omitempty"  db:"bulk"`
	Date        time.Time `json:"added,omitempty"  db:"added"`
}

type AppError struct {
	Status int16       `json:"status"`
	Err    interface{} `json:"err_text"`
}

type JsonResponse struct {
	Scan     interface{} `json:"sent"`
	Detect   string      `json:"scan_detect,omitempty"  db:"scan_detect"`
	Duration int         `json:"duration,omitempty"  db:"duration"`
	Result   string      `json:"scan_result,omitempty"  db:"scan_result"`
	Status   string      `json:"scan_status,omitempty"  db:"scan_status"`
	AppErr   AppError    `json:"app_error,omitempty"`
}

var evdb *redis.Client

func NewItem(s *Scan) {
	ctx := context.TODO()
	if err := evdb.MSet(ctx, "FilePath", s.FilePath, "User", s.User, "data", s.Data, "Description", s.Description); err != nil {
		log.Panicln(err)
	}
	//evdb.Do("EXPIRE", key, ttl)

}

func LookItem(id int64) (found Scan, rerr error) {
	ctx := context.TODO()
	val, err := evdb.LIndex(ctx, "scans", id).Result()
	switch {
	case err == redis.Nil:
		rerr = errors.New("key does not exist")
	case err != nil:
		rerr = errors.New("failed to obain")
	case val == "":
		rerr = errors.New("value is empty")
	}
	if err := json.Unmarshal([]byte(val), found); err != nil {
		rerr = err
	}
	return
}

// A map to store the books with the ISDN as the key
// This acts as the storage in lieu of an actual database
var fileScan = make(map[string]*Scan)

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	response := &JsonResponse{}
	fmt.Fprint(w, "read docs, printing all in redis\n")
	response.Result = "index"
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(response); err != nil {
		log.Panicln(err)
	}
}

// new virus scan of file
// POST /scan
func NewScan(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	fScan := &Scan{}
	response := &JsonResponse{}
	full, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		log.Panicln(err)
		log.Printf("error reading incoming request body to memory")
	}
	if err := json.NewDecoder(bytes.NewBuffer(full)).Decode(&fScan); err != nil {
		log.Println("error at json, responding as such")
		response.AppErr = AppError{Status: 405, Err: "Method Not Allowed"}
		if err := json.NewEncoder(w).Encode(response); err != nil {
			// For us to review later
			log.Panicln(err)
		}

		return
	}
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(response); err != nil {
		log.Panicln(err)
	}
}

// GET /scan/:id
func GetScan(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	//w.Header().Set("Access-Control-Allow-Origin", "*")
	iId, _ := strconv.Atoi(params.ByName("id"))
	response := &JsonResponse{}

	if scanId, ok := LookItem(int64(iId)); ok != nil {
		w.WriteHeader(http.StatusNotFound)
		response.AppErr = AppError{Status: 404, Err: "Item Not Found"}
		if err := json.NewEncoder(w).Encode(response); err != nil {
			log.Panicln(err)
		}
	} else {
		// caution, might not be hit
		response.Scan = scanId
	}
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(response); err != nil {
		log.Panicln(err)
	}
}

func main() {

	router := httprouter.New()
	router.GET("/", Index)
	router.GET("/scan/:id", GetScan)
	router.POST("/scan/", NewScan)

	log.Fatal(http.ListenAndServe(os.Getenv("APP_PORT"), router))
}
